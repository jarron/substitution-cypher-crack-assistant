#TODO: show which chars have been changed
#TODO: change text input wihout restart
#TODO: Show warning when  char is already substituted in
import re

cypher = input("Enter the encoded text or the path to a text file: ")
regexQuery = ".=."
fileRegex = ".+\.txt$"
chars = []
frequencies = {}
substitutes = []

if(re.match(fileRegex, cypher)):
    try:
        reader = open(cypher)
        cypher = reader.read()
    except:
        print("Could not read the file, check if it exists and is a text file.")
    finally:
        reader.close()

[chars.append([char,char]) for char in cypher]

def printCypherText():
    origChars = ""
    for index, i in enumerate(chars):
        print(i[0], end="")
        origChars += str(i[1])
        if i[1] == '\n' or index == (len(chars)-1):
            if i[1] != '\n':
                print('')
            print("\x1b[38;2;255;0;0m", end="")
            print(origChars, end='')
            print("\x1b[0m", end='')
            origChars = ""
    print('')

def frequency():
    for char in chars:
        if char[1] != '\n':
            frequencies[char[1]] = frequencies.get(char[1], 0) + 1    
    

frequency()

while True:
    command = input("> ")
    if(command == "frequency" or command == "f"):
        [print(str(k) + ":", "'" + str(v) + "'") for (k,v) in sorted([(v,k) for (k,v) in frequencies.items()], reverse=True)]

    elif(command == "subs" or command == "s"): [print(x) for x in substitutes]

    elif(command == "print" or command == "p"): printCypherText()

    elif(command == "help" or command == "h" or command == "?"):
        print('''
    x=y - Substitute the letter x with the letter y in the cypher text. Letter 1 must always be the original letter shown in the red text below the line with subsitutions made.
    print (p) - Print the text with substitutions made and the original text.
    subs (s) - Show substitutions made.
    frequency (f) - Show the breakdown of the frequencies of characters in the cypher text.
    help (h,?) - Show this information.
             ''')

    elif(re.match(regexQuery, command)):


        #TODO: if substitutes[i][-1] == command[0] remove

        for sub in substitutes:
            if sub[0].lower() == command[0].lower():
                substitutes.remove(sub)
        if(command[0] != command[-1]):
            substitutes.append(command) 

        for i in chars:
            #TODO: maybe keep case when replacing?
            if i[1].lower() == command[0].lower():
                i[0] = command[-1]
        
        printCypherText()